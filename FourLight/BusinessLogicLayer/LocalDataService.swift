//
//  LocalDataService.swift
//  FourLight
//
//  Created by Pavel Tertyshnyy on 29/12/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import Foundation

internal protocol LocalDataService {
    func getHelps() -> [Help]
}
