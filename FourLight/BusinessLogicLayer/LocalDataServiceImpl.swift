//
//  LocalDataServiceImpl.swift
//  FourLight
//
//  Created by Pavel Tertyshnyy on 29/12/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import Foundation

internal class LocalDataServiceImpl: LocalDataService {
    
    private init() { }
    static let shared: LocalDataServiceImpl = LocalDataServiceImpl()
    
    func getHelps() -> [Help] {
        
        if let path = Bundle.main.path(forResource: "HelpData", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                    
                return decode(data: data, type: [Help].self) ?? []
            } catch {
                // handle error
            }
        }
            
        return []
    }
}
