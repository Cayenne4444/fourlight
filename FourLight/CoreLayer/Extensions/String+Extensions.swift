//
//  String+Extensions.swift
//  FourLight
//
//  Created by Pavel Tertyshnyy on 21/12/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import UIKit

extension String {
    
    func height(withConstrained width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin],
                                            attributes: [NSAttributedString.Key.font: font
                                                /*,NSAttributedString.Key.kern: 2.0*/], context: nil)
        return boundingBox.height
    }
}
