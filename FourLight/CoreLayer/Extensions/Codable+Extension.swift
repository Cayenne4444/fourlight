//
//  Codable+Extension.swift
//  FourLight
//
//  Created by Pavel Tertyshnyy on 16/12/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import Foundation

typealias JSON = [String : Any]

protocol Serializable: Codable {
    func encode() -> JSON?
}

extension Serializable {
    
    func encode() -> JSON? {
        
        do {
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            encoder.keyEncodingStrategy = .convertToSnakeCase
            
            let encodedData = try encoder.encode(self)
            let json = try JSONSerialization.jsonObject(with: encodedData, options: [])
            
            return json as? JSON
            
        } catch let error {
            
            print("Failed to encode:", error)
            return nil
        }
    }
}

public func decode<T:Codable>(data: Data, type: T.Type) -> T? {
    
    do {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        return try decoder.decode(type, from: data)
    } catch let error {
        
        print("Failed to decode:", error)
        return nil
    }
}

