//
//  HelpInteractiveViewModel.swift
//  FourLight
//
//  Created by Pavel Tertyshnyy on 22/12/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import UIKit

protocol HelpInteractiveViewModelType: class {
    var description: Box<String> { get }
    var label: Box<String> { get }
}

class HelpInteractiveViewModel: HelpInteractiveViewModelType {
    
    private var questions: InteractiveHelp
    
    init(questions: InteractiveHelp) {
        self.questions = questions
    }
    
    var description: Box<String> {
        return Box(questions.description)
    }
    
    var label: Box<String> {
        return Box(questions.title)
    }
}
