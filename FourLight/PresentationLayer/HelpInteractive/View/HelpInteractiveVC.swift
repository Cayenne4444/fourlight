//
//  HelpInteractiveVC.swift
//  FourLight
//
//  Created by Pavel Tertyshnyy on 18/12/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import UIKit
import SnapKit

class HelpInteractiveVC: UIViewController {
    
    // MARK: - Properties
    
    weak var viewModel: HelpInteractiveViewModelType? {
        didSet {
            viewModel?.description.bind { [weak self] in self?.descriptionTextView.text = $0 }
            viewModel?.label.bind { [weak self] in self?.answerLabel.text = $0 }
        }
    }
    
    var descriptionTextView: UITextView = {
        let textView = UITextView()
        textView.textColor = .gray
        textView.font = UIFont(name: "Helvetica", size: 24)
        
        return textView
    }()
    
    var answerLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "Я знаю"
        label.textAlignment = .center
        
        return label
    }()
    
    var noAnswerButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .red
        button.layer.cornerRadius = 10.0
        button.clipsToBounds = true
        
        return button
    }()
    
    var foundAnswerButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .green
        button.layer.cornerRadius = 10.0
        button.clipsToBounds = true
        
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        view.addSubview(descriptionTextView)
        view.addSubview(answerLabel)
        view.addSubview(noAnswerButton)
        view.addSubview(foundAnswerButton)
        
        noAnswerButton.addTarget(self, action: #selector(noAnswerButtonAction), for: .touchUpInside)
        foundAnswerButton.addTarget(self, action: #selector(foundAnswerButtonAction), for: .touchUpInside)
        
        makeConstraints()
    }
    
    // MARK: - Layout
    
    private func makeConstraints() {
        
        noAnswerButton.snp.makeConstraints { make in
            make.right.equalTo(view.snp.centerX)
            if #available(iOS 11.0, *) {
                make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).inset(32.0)
            } else {
                make.bottom.equalToSuperview().inset(32.0)
            }
            make.width.equalTo(120.0)
            make.height.equalTo(44.0)
        }
        foundAnswerButton.snp.makeConstraints { make in
            make.left.equalTo(view.snp.centerX)
            make.bottom.equalTo(noAnswerButton.snp.bottom)
            make.width.equalTo(120.0)
            make.height.equalTo(44.0)
        }
        answerLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(16.0)
            make.right.equalToSuperview().inset(16.0)
            make.bottom.equalTo(noAnswerButton.snp.top).offset(-32.0)
        }
        descriptionTextView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(16.0)
            make.right.equalToSuperview().inset(16.0)
            make.bottom.equalTo(answerLabel.snp.top).offset(-16.0)
            if #available(iOS 11.0, *) {
                make.top.equalTo(view.safeAreaLayoutGuide.snp.top).inset(32.0)
            } else {
                make.top.equalToSuperview().inset(32.0)
            }
        }
        
    }
    
    @objc
    func noAnswerButtonAction(_ sender: UIButton) {
        
    }
    
    @objc
    func foundAnswerButtonAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
