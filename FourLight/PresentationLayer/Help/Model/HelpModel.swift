//
//  Help.swift
//  ForeLite
//
//  Created by Pavel Tertyshnyy on 25/11/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import UIKit

struct Help: Codable, Serializable {
    var title: String
    var steps: [StepHelp]
    var questions: [InteractiveHelp]?
}

struct StepHelp: Codable {
    var description: String
    var image: String?
}

struct InteractiveHelp: Codable {
    var title: String
    var description: String
}
