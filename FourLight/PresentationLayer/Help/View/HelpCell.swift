//
//  HelpCell.swift
//  FourLight
//
//  Created by Pavel Tertyshnyy on 08/12/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import UIKit

class HelpCell: UITableViewCell {
    
    // MARK: - Properties
    
    weak var viewModel: TableViewCellViewModelType? {
        didSet {
            viewModel?.title.bind { [weak self] in self?.titleLable.text = $0 }
        }
    }
    
    let titleLable: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: 16.0)
        label.textColor = .gray
        label.textAlignment = .left
        
        return label
    }()
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(titleLable)
        makeConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Layout
    
    private func makeConstraints() {
        
        titleLable.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(16.0)
            make.right.equalToSuperview().offset(-16.0)
            make.centerY.equalToSuperview()
        }
    }
}
