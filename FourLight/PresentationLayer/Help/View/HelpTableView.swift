//
//  HelpTableView.swift
//  FourLight
//
//  Created by Pavel Tertyshnyy on 08/12/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import UIKit

class HelpTableView: UITableView {
    
    init() {
        super.init(frame: .zero, style: .plain)
        backgroundColor = .clear
        register(HelpCell.self, forCellReuseIdentifier: "HelpCell")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
