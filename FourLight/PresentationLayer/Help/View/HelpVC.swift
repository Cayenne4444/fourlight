//
//  ViewController.swift
//  ForeLite
//
//  Created by Pavel Tertyshnyy on 25/11/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import UIKit
import SnapKit

class HelpVC: UIViewController {
    
    // MARK: - Properties
    
    let tableView: UITableView = HelpTableView()
    
    private var viewModel: HelpViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        viewModel = HelpViewModel()
        
        view.backgroundColor = .white
        view.addSubview(tableView)
        
        makeConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let index = self.tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: index, animated: true)
        }
    }
    
    // MARK: - Layout
    
    private func makeConstraints() {
        
        tableView.snp.makeConstraints { make in
            make.left.right.top.bottom.equalToSuperview()
        }
    }
}

extension HelpVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return dequeueHelpCell(tableView, forRowAt: indexPath)
    }
    
    private func dequeueHelpCell(_ tableView: UITableView, forRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HelpCell", for: indexPath) as? HelpCell
        
        guard let tableCell = cell else { return UITableViewCell() }
        
        let cellViewModel = viewModel.cellViewModel(forIndexPath: indexPath)
        tableCell.viewModel = cellViewModel
        
        return tableCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let viewModel = viewModel else { return }
        viewModel.selectRow(atIndexPath: indexPath)
        
        #warning("Роутинг или билдер или где")
        let viewController = HelpDetailVC()
        viewController.viewModel = viewModel.viewModelForSelectRow()

        present(viewController, animated: true, completion: nil)
        
//        let viewController = HelpInteractiveVC()
//
//        present(viewController, animated: true, completion: nil)
    }
}
