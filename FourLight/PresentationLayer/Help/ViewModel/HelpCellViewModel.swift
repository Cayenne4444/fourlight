//
//  TableViewCellViewModel.swift
//  FourLight
//
//  Created by Pavel Tertyshnyy on 08/12/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import Foundation

protocol TableViewCellViewModelType: class {
    var title: Box<String> { get }
}

class HelpCellViewModel: TableViewCellViewModelType {
    
    private var help: Help
    
    var title: Box<String> {
        return Box(help.title)
    }
    
    init(help: Help) {
        self.help = help
    }
}

