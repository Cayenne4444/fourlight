//
//  HelpViewModel.swift
//  ForeLite
//
//  Created by Pavel Tertyshnyy on 25/11/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import Foundation

class HelpViewModel {
    
    private var selectedIndexPath: IndexPath?
    
    #warning("Узнать насколько это клево вот")
    private var helps: [Help] = []
    
    init(service: LocalDataService) {
        helps = service.getHelps()
    }
    
    func numberOfRows() -> Int {
        return helps.count
    }
    
    func cellViewModel(forIndexPath indexPath: IndexPath) -> TableViewCellViewModelType? {
        let help = helps[indexPath.row]
        return HelpCellViewModel(help: help)
    }
    
    func viewModelForSelectRow() -> HelpDetailViewModel? {
        guard let selectedIndexPath = selectedIndexPath else {
            return nil
        }
        return HelpDetailViewModel(steps: helps[selectedIndexPath.row].steps)
    }
    
    func selectRow(atIndexPath indexPath: IndexPath) {
        self.selectedIndexPath = indexPath
    }
}

