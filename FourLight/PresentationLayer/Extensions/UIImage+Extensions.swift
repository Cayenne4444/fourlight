//
//  UIImage+Extensions.swift
//  FourLight
//
//  Created by Pavel Tertyshnyy on 20/12/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import UIKit

extension UIImage {

    func getSize(for side: CGFloat) -> CGFloat {
        
        let imageWidth:  CGFloat = self.size.width
        let imageHeight: CGFloat = self.size.height
        var ratio: CGFloat = 0.0
        
        if imageWidth > imageHeight {
            ratio = imageWidth / imageHeight
            return side / ratio
        } else {
            ratio = imageHeight / imageWidth
            return side * ratio
        }
    }
    
}
