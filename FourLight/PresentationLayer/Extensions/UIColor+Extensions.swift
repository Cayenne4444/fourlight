//
//  UIColor.swift
//  FourLight
//
//  Created by Pavel Tertyshnyy on 08/12/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import UIKit

extension UIColor {
    
    @nonobjc public class var lightOrange: UIColor {
        return UIColor(named: "Orange")!
    }
}
