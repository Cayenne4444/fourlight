//
//  UIFont.swift
//  FourLight
//
//  Created by Pavel Tertyshnyy on 27/11/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import UIKit

extension UIFont {
    
    static func SFProDisplayBold(_ fontSize: CGFloat) -> UIFont {
        return UIFont(name: "SF-Pro-Display-Bold", size: fontSize)!
    }
    
}
