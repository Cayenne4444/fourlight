//
//  HelpDetailVC.swift
//  FourLight
//
//  Created by Pavel Tertyshnyy on 08/12/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import UIKit
import SnapKit

class HelpDetailVC: UIViewController {
    
    // MARK: - Properties
    
    let tableView: UITableView = HelpDetailTableView()
    
    var backButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "exitIcon"), for: .normal)
        button.layer.cornerRadius = 22.0
        button.clipsToBounds = true
        
        return button
    }()
    
    var poroshImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "noImage")
        
        return imageView
    }()
    
    var viewModel: HelpDetailViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        view.backgroundColor = .white
        
        view.addSubview(poroshImageView)
        view.addSubview(tableView)
        view.addSubview(backButton)
        
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        makeConstraints()
    }
    
    // MARK: - Layout
    
    private func makeConstraints() {
        
        poroshImageView.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.height.equalTo(poroshImageView.snp.width).multipliedBy(350.0 / 319.0)
            if #available(iOS 11.0, *) {
                make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            } else {
                make.top.equalToSuperview()
            }
        }
        backButton.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(8.0)
            if #available(iOS 11.0, *) {
                make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(4.0)
            } else {
                make.top.equalToSuperview().offset(44.0)
            }
            make.width.height.equalTo(44.0)
        }
        tableView.snp.makeConstraints { make in
            make.left.right.top.bottom.equalToSuperview()
        }
    }
    
    // MARK: - Actions
    
    @objc
    func backAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension HelpDetailVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRows() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return dequeueHelpCell(tableView, forRowAt: indexPath)
    }
    
    private func dequeueHelpCell(_ tableView: UITableView, forRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HelpDetailCell", for: indexPath) as? HelpDetailCell
        
        guard let tableCell = cell, let viewModel = viewModel else { return UITableViewCell() }
    
        let cellViewModel = viewModel.cellViewModel(forIndexPath: indexPath)
        tableCell.viewModel = cellViewModel
        
        return tableCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        guard let viewModel = viewModel else { return 0.0 }
        
        return viewModel.heightForRowViewModel(size: view.frame.width, forIndexPath: indexPath)
    }
}
