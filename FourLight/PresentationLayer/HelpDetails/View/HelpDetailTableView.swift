//
//  HelpDetailTableView.swift
//  FourLight
//
//  Created by Pavel Tertyshnyy on 08/12/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import UIKit

class HelpDetailTableView: UITableView {
    
    init() {
        super.init(frame: .zero, style: .plain)
        backgroundColor = .clear
        register(HelpDetailCell.self, forCellReuseIdentifier: "HelpDetailCell")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
