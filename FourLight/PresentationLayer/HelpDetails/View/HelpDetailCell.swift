//
//  HelpDetailCell.swift
//  FourLight
//
//  Created by Pavel Tertyshnyy on 08/12/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import UIKit

class HelpDetailCell: UITableViewCell {
    
    // MARK: - Properties
    
    weak var viewModel: HelpDetailCellViewModelType? {
        didSet {
            viewModel?.description.bind { [weak self] in self?.descriptionLable.text = $0 }
            viewModel?.image.bind { [weak self] in
                if let name = $0, let image = UIImage(named: name) {
                    self?.detailImageView.image = image
                }
            }
        }
    }

    private let detailImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        
        return imageView
    }()
    
    private let descriptionLable: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: 16.0)
        label.textColor = .gray
        label.textAlignment = .left
        label.numberOfLines = 0
        
        return label
    }()
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none

        contentView.addSubview(detailImageView)
        contentView.addSubview(descriptionLable)
        
        makeConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Layout
    
    private func makeConstraints() {
        
        detailImageView.snp.makeConstraints { make in
            make.top.left.right.equalToSuperview()
        }
        descriptionLable.snp.makeConstraints { make in
            make.top.equalTo(detailImageView.snp.bottom).offset(16.0)
            make.left.equalToSuperview().offset(16.0)
            make.right.equalToSuperview().inset(16.0)
            make.bottom.equalToSuperview().inset(16.0)
        }
    }
    
}

