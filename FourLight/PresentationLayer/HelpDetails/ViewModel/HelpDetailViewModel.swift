//
//  HelpDetailViewModel.swift
//  FourLight
//
//  Created by Pavel Tertyshnyy on 08/12/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import UIKit

class HelpDetailViewModel {
    
    private var steps: [StepHelp]
    
    init(steps: [StepHelp]) {
        self.steps = steps
    }
    
    func numberOfRows() -> Int {
        return steps.count
    }
    
    func cellViewModel(forIndexPath indexPath: IndexPath) -> HelpDetailCellViewModelType {
        let step = steps[indexPath.row]
        return HelpDetailCellViewModel(step: step)
    }
    
    func heightForRowViewModel(size: CGFloat, forIndexPath indexPath: IndexPath) -> CGFloat {
        let step = steps[indexPath.row]
        var rowHeight: CGFloat = 0.0
        
        if let name = step.image, let image = UIImage(named: name) {
            rowHeight = image.getSize(for: size)
            rowHeight += 16.0
        }
        
        rowHeight += step.description.height(
            withConstrained: size - 32.0,
            font: UIFont(name: "Helvetica", size: 16.0)!
        )
        rowHeight += 16.0
        
        return rowHeight
    }
    
}
