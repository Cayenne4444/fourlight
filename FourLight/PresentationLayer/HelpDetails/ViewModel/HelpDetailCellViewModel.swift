//
//  HelpDetailCellViewModel.swift
//  FourLight
//
//  Created by Pavel Tertyshnyy on 16/12/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import UIKit

protocol HelpDetailCellViewModelType: class {
    var description: Box<String> { get }
    var image: Box<String?> { get }
}

class HelpDetailCellViewModel: HelpDetailCellViewModelType {
    
    private var step: StepHelp
    
    var description: Box<String> {
        return Box(step.description)
    }
    
    var image: Box<String?> {
        return Box(step.image)
    }
    
    init(step: StepHelp) {
        self.step = step
    }
}
