//
//  AppDelegate.swift
//  ForeLite
//
//  Created by Pavel Tertyshnyy on 25/11/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // MARK: - Init start view controller
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let mainViewController = HelpVC()
        
        window?.rootViewController = mainViewController
        window?.makeKeyAndVisible()
        
        return true
    }
    
}

