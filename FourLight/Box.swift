//
//  Box.swift
//  ForeLite
//
//  Created by Pavel Tertyshnyy on 25/11/2018.
//  Copyright © 2018 Pavel Tertyshnyy. All rights reserved.
//

import Foundation

class Box<T> {
    
    typealias Listener = (T) -> ()
    
    private var listeners: [Listener] = []
    
    var value: T {
        didSet {
            for l in listeners {
                l(value)
            }
        }
    }
    
    func bind(listener: @escaping Listener) {
        listeners.append(listener)
        listener(value)
    }
    
    func addListener(listener: @escaping Listener) {
        listeners.append(listener)
    }
    
    init(_ value: T) {
        self.value = value
    }
}

